var request = require('request');

var { scheduleModel } = require('../models/models.js')

const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_API_KEY)

var Schedules = function (app, router) {
    this.app = app
    this.router = router
}

module.exports = Schedules

Schedules.prototype.scheduleAction = function (req, res) {

    const self = this

    var action = req['params']['action']

    if (action === 'add-schedule') {
        self.addScheduleEmail(req, res)
    } else if (action === 'list') {
        self.listSchedules(req, res)
    } else if (action === 'delete') {
        self.deleteSchedules(req, res)
    } else if (action === 'reschedule') {
        self.rescheduleEmail(req, res)
    } else if (action === 'email-unsent-users') {
        self.emailUnsentUsers(req, res)
    }
}


Schedules.prototype.addScheduleEmail = function (req, res) {

    var reqObj = req.body

    request.post({
        uri: 'https://api.sendgrid.com/v3/mail/batch',
        headers: { 'Authorization': 'Bearer ' + process.env.SENDGRID_API_KEY },
    }, function (err, result) {
        if (!err) {
            var batchId = JSON.parse(result.body).batch_id
            reqObj['batch_id'] = batchId
            //Schedule Details added into schedule collection in DB
            scheduleModel.insertMany(reqObj, function (err, result) {
                if (!err) {
                    //Create Schedule message
                    const msg = {
                        to: reqObj.email,
                        from: 'sureshsks477@gmail.com',
                        subject: 'Interview Task',
                        html: '<strong>Hi BigApp..!</strong>',
                        send_at: reqObj.send_at,
                        batch_id: batchId
                    }
                    sgMail.send(msg, function (emailErr, emailResult) {
                        if (!emailErr) {
                            res.json({ status: true, result: emailResult, message: "Email sent successfully" })
                        } else {
                            res.json({ status: false, result: emailErr, message: "Error in sent email" })
                        }
                    })
                } else {
                    res.json({ status: false, result: err, message: "Error in adding scheduled email details" })
                }
            })
        } else {
            res.json({ status: false, result: result, message: "Error in creating batch id" })
        }
    })

}

//List Schedule details from schedule collection

Schedules.prototype.listSchedules = async function (req, res) {

    var reqobj = req.body

    const self = this;

    scheduleModel.find({}, function (err, result) {
        if (!err) {
            res.json({ status: true, result: result, message: "Scheduled email details fetched successfully" })
        } else {
            res.json({ status: false, result: result, message: "Error in fetching scheduled email details" })
        }
    })
}


Schedules.prototype.rescheduleEmail = function (req, res) {

    var reqObj = req.body

    var batchId = req.body.batch_id

    var userId = req.body._id

    var updateObj = {
        "batch_id": batchId,
        "status": "cancel"
    }

    request.post({
        uri: 'https://api.sendgrid.com/v3/user/scheduled_sends',
        headers: {
            'Authorization': 'Bearer ' + process.env.SENDGRID_API_KEY,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(updateObj)
    }, function (err, result) {
        if (!err) {
            const msg = {
                to: reqObj.email,
                from: 'sureshsks477@gmail.com',
                subject: 'Interview Task',
                html: '<strong>Hi BigApp..!</strong>',
                send_at: reqObj.send_at,
                batch_id: batchId
            }
            sgMail.send(msg, function (emailErr, emailResult) {
                if (!emailErr) {
                    scheduleModel.findByIdAndUpdate(userId, reqObj, { useFindAndModify: false }, function (err, result) {
                        if (!err) {
                            res.json({ status: true, result: result, message: "Scheduled email details updated successfully" })
                        } else {
                            res.json({ status: false, result: result, message: "Error in update scheduled email details" })
                        }
                    })
                } else {
                    res.json({ status: false, result: emailErr, message: "Error in cancel schedules" })
                }
            })
        } else {
            res.json({ status: false, result: result, message: "Error in cancel schedules" })
        }
    })
}

Schedules.prototype.deleteSchedules = function (req, res) {

    var userId = req.body._id

    var batchId = req.body.batch_id

    var updateObj = {
        "batch_id": batchId,
        "status": "cancel"
    }

    request.post({
        uri: 'https://api.sendgrid.com/v3/user/scheduled_sends',
        headers: {
            'Authorization': 'Bearer ' + process.env.SENDGRID_API_KEY,
            'Content-type': 'application/json'
        },
        body: JSON.stringify(updateObj)
    }, function (err, result) {
        if (!err) {
            scheduleModel.findByIdAndDelete(userId, { useFindAndModify: false }, function (err, result) {
                if (!err) {
                    res.json({ status: true, result: result, message: "Scheduled email details deleted successfully" })
                } else {
                    res.json({ status: false, result: result, message: "Error in delete scheduled email details" })
                }
            })
        } else {
            res.json({ status: false, result: result, message: "Error in cancel schedules" })
        }
    })
}


Schedules.prototype.emailUnsentUsers = function (req, res) {

    var reqObj = req.body

    var intervalObj = {
        "start_time": reqObj.start_time,
        "end_time": reqObj.end_time
    }

    request.get({
        uri: 'https://api.sendgrid.com/v3/suppression/bounces',
        headers: {
            'Authorization': 'Bearer ' + process.env.SENDGRID_API_KEY,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(intervalObj),
    }, function (err, result) {
        if (!err && result.statusCode === 200) {
            res.json({ status: true, result: JSON.parse(result.body), message: "Unsend scheduled emails list fetched successfully" })
        } else {
            res.json({ status: false, result: err, message: "Error in fetching unsend scheduled emails" })
        }
    })
}