const mongoose = require('mongoose')

const scheduleSchema = mongoose.Schema({
    "first_nam": {
        "type": String,
        "required": false,
    },
    "last_nam": {
        "type": String,
        "required": false,
    },
    "address": {
        "type": String,
        "required": false,
    },
    "phone": {
        "type": String,
        "required": false,
    },
    "email": {
        "type": String,
        "required": true,
        "unique": false
    },
    "send_at": {
        "type": Number,
        "required": true
    },
    "batch_id": {
        "type": String
    }
})

const scheduleModel = mongoose.model('schedule', scheduleSchema)

module.exports = { scheduleModel }



