var Schedules = require('../modules/schedules.js')


var APIRoutes = function (app, router) {
    this.app = app;
    this.router = router
    this.schedules = new Schedules(app, router)
    this.init()
}

module.exports = APIRoutes

APIRoutes.prototype.init = function (req, res, next) {

    const self = this

    self.router.post('/schedule/:action', function (req, res) {
        self.schedules.scheduleAction(req, res)
    })

    self.app.use("/big-app", self.router)
}