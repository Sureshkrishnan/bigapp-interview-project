// Import Required Modules

const express = require('express')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const path = require('path')
const app = express()
const router = express.Router()
const layout = require('express-layout');


const conf = require('./config.js')

//Use Body Parser
app.use(bodyParser.json())
app.use(cookieParser("BIGAPP"))
app.conf = conf

//For static path 
app.set('views',path.join(__dirname,'views'))

//For Template Engine
app.engine('html',require('ejs').renderFile)
app.set("view engine",'html')


//Port No
var port = 8081


//Server Node 
var server = require('http').Server(app)
console.log("Node listening on " + port)
server.listen(port)


//API route
var APIRoutes = require('./routes/api-routes')
new APIRoutes(app, router)